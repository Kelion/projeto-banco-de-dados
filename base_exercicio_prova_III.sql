CREATE DATABASE IF NOT EXISTS livraria_teste;
USE livraria_teste;

CREATE TABLE IF NOT EXISTS Cliente (
	codigo int,
	nome varchar(100) not null,
    email varchar(100), 	
    rg char(8) not null,		-- apenas os números.
	rua varchar(100),
    cep char(9) not null, 		-- seguindo o padrão 'XXXXXX-000'.
    bairro varchar(100),
    constraint pk_Cliente primary key (codigo),
    unique (rg)
);

CREATE TABLE IF NOT EXISTS Editora (
	codigo int,
	nome varchar(50) not null,
    constraint pk_Editora primary key (codigo),
    unique (nome)
);

CREATE TABLE IF NOT EXISTS Autor (
	codigo int,
    email varchar(100),
    nome varchar(100) not null,
    constraint pk_Autor primary key (codigo)
);

CREATE TABLE IF NOT EXISTS Livro (
	ISBN char(13),
    edicao int,
    custo decimal(7,2) not null,
    titulo varchar(100) not null,
    codigoEditora int,		-- criar foreign key
    constraint pk_Livro primary key (ISBN),
    constraint fk_Editora_codigo foreign key (codigoEditora) references Editora (codigo)
);

CREATE TABLE IF NOT EXISTS Exemplar (
	codigo int,
	dataAquisicao date,		-- YYYY/MM/DD
    livroISBN char(13), 	-- criar foreign key
    constraint pk_Exemplar primary key (codigo),
    constraint fk_Livro_codigo foreign key (livroISBN) references Livro (ISBN)
);

CREATE TABLE IF NOT EXISTS Autoria (
	autorCodigo int not null,
    livroISBN char(13),		-- criar foreign key
    constraint fk_Autor_codigo foreign key (autorCodigo) references Autor (codigo),
    constraint fk_Livro_ISBN foreign key (livroISBN) references Livro (ISBN)
);

CREATE TABLE IF NOT EXISTS Emprestimo (
	dataEmprestimo date, 			-- YYYY/MM/DD
	clienteCodigo int not null,		-- criar foreign key
    exemplarCodigo int not null,	-- criar foreign key
    constraint pk_Emprestimo primary key (dataEmprestimo),
    constraint fk_Cliente_codigo foreign key (clienteCodigo) references Cliente (codigo),
	constraint fk_Exemplar_codigo foreign key (exemplarCodigo) references Exemplar (codigo)
);