CREATE DATABASE IF NOT EXISTS livraria_teste;
USE livraria_teste;

CREATE TABLE IF NOT EXISTS cliente (
	codigo int,
	nome varchar(100) not null,
    email varchar(100), 	
    rg char(8) not null,	-- apenas os números.
	rua varchar(100),
    cep varchar(9), 		-- seguindo o padrão 'XXXXXX-000'.
    bairro varchar(100),
    constraint pk_cliente primary key (codigo),
    unique (rg)
);

CREATE TABLE IF NOT EXISTS editora (
	codigo int,
	nome varchar(50) not null,
    constraint pk_editora primary key (codigo),
    unique (nome)
);

CREATE TABLE IF NOT EXISTS autores (
	codigo int,
    email varchar(100),
    nome varchar(100) not null,
    constraint pk_autores primary key (codigo)
);

CREATE TABLE IF NOT EXISTS livros (
	ISBN char(13),
    edicao int,
    custo decimal(7,2) not null,
    titulo varchar(100) not null,
    codigoEditora int,
    constraint pk_livros primary key (ISBN),
    constraint fk_Editora_codigo foreign key (codigoEditora) references editora (codigo)
);

CREATE TABLE IF NOT EXISTS exemplares (
	codigo int,
	dataAquisicao date,
    livrosISBN char(13), 
    constraint pk_exemplares primary key (codigo),
    constraint fk_Livros_codigo foreign key (livrosISBN) references livros (ISBN)
);

CREATE TABLE IF NOT EXISTS autoria (
	autoresCodigo int not null,
    livrosISBN char(13),
    constraint fk_Autores_codigo foreign key (autoresCodigo) references autores (codigo),
    constraint fk_Livros_ISBN foreign key (livrosISBN) references livros (ISBN)
);

CREATE TABLE IF NOT EXISTS emprestimo (
	dataEmprestimo date, 
	clienteCodigo int not null,
    exemplaresCodigo int not null,
    constraint pk_emprestimo primary key (dataEmprestimo),
    constraint fk_Cliente_codigo foreign key (clienteCodigo) references cliente (codigo),
	constraint fk_Exemplares_codigo foreign key (exemplaresCodigo) references exemplares (codigo)
);

