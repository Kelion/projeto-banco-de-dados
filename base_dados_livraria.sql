-- Equipe: 
-- Kelion Fernandes Aquino - 10772979
-- Emanuel Alexandre Carvalho - 23795891
-- Larissa Nahan Santos Diniz Gadelha Dantas - 10768041

CREATE DATABASE IF NOT EXISTS livraria_dados_livros;
USE livraria_dados_livros;

CREATE TABLE IF NOT EXISTS Editora (
	codigo int,
	nome varchar(50) not null,
    constraint pk_Editora primary key (codigo),
    unique (nome)
);

--    codigo, nome
insert into Editora values(5760, 'Alta Books');
insert into Editora values(5961, "O'Reilly Media");
insert into Editora values(5352, "GEN LTC");
insert into Editora values(5752, "Novatec");
insert into Editora values(1188, 'For Dummies');
insert into Editora values(5212, "Blucher");
insert into Editora values(5365, "Érica");
insert into Editora values(5277, "Wiley-VCH");
insert into Editora values(5510, 'Intrínseca');
insert into Editora values(3051, 'Cengage Learning');

select * from Editora;


CREATE TABLE IF NOT EXISTS Autor (
	codigo int,
    email varchar(100),
    nome varchar(100) not null,
    constraint pk_Autor primary key (codigo)
);

--    codigo, email, nome
insert into Autor values(1, 'cjdate@email.com', 'C. J. Date');
insert into Autor values(2, 'lucy10ramalho@email.com', 'Luciano Ramalho');
insert into Autor values(3, 'pablodallo@email.com', "Pablo Dall'Oglio");
insert into Autor values(4, 'stephen_r_davis@email.com', 'Stephen R. Davis');
insert into Autor values(5, 'neymenezes@email.com', 'Nilo Ney Coutinho Menezes');
insert into Autor values(6, 'mickey_dalson@email.com', 'Michael Dawson');
insert into Autor values(7, 'scotty_roggy@email.com', 'Scott Rogers');
insert into Autor values(8, 'arnoldo1959@email.com', 'Arnold Robbins');
insert into Autor values(9, 'jayr_oliveira@email.com', 'Jayr Figueiredo de Oliveira');
insert into Autor values(10, 'jon_duck@email.com', 'Jon Duckett');

select * from Autor;


CREATE TABLE IF NOT EXISTS Livro (
	ISBN char(13),
    edicao int,
    custo decimal(7,2) not null,
    titulo varchar(100) not null,
    codigoEditora int,
    constraint pk_Livro primary key (ISBN),
    constraint fk_Editora_codigo foreign key (codigoEditora) references Editora (codigo)
);

--    ISBN, edicao, custo, titulo, codigoEditora
insert into Livro values('9788535212730', 8, 326.03, 'Introdução a Sistemas de Bancos de Dados', 5352);
insert into Livro values('9788575226919', 4, 89.14, 'PHP Programando com Orientação a Objetos', 5752);
insert into Livro values('9788576089452', 1, 140.89, 'Javascript e Jquery: Desenvolvimento de Interfaces web Interativas', 5760);
insert into Livro values('9788575227183', 3, 42.30, 'Introdução à Programação com Python: Algoritmos e Lógica de Programação Para Iniciantes', 5752);
insert into Livro values('9780596100292', 4, 228.96, 'UNIX in a Nutshell: A Desktop Quick Reference - Covers Gnu/Linux, Mac OS X, and Solaris', 5961);
insert into Livro values('9788576089964', 7, 66.99, 'C++ Para Leigos', 5760);
insert into Livro values('9788536531458', null, 71.11, 'Algoritmos: Lógica Para Desenvolvimento de Programação de Computadores', 5365);
insert into Livro values('9781305109919', 4, 188.73, 'Beginning C++ Through Game Programming', 3051);
insert into Livro values('9783527760701', 1, 410.48, 'PHP & MySQL', 5277);
insert into Livro values('9788521207009', 1, 107.28, 'Level UP: um Guia Para o Design de Grandes Jogos', 5212);

select * from Livro;


CREATE TABLE IF NOT EXISTS Exemplar (
	codigo int,
	dataAquisicao date,
    livroISBN char(13), 
    constraint pk_Exemplar primary key (codigo),
    constraint fk_Livro_codigo foreign key (livroISBN) references Livro (ISBN)
);

--    codigo, dataAquisicao, livroISBN
insert into Exemplar values(1, '2016/01/31', '9788576089452');
insert into Exemplar values(2, '2016/04/29', '9788536531458');
insert into Exemplar values(3, '2018/08/10', '9788575226919');
insert into Exemplar values(4, '2014/06/23', '9781305109919');
insert into Exemplar values(5, '2015/04/14', '9788535212730');
insert into Exemplar values(6, '2005/10/01', '9780596100292');
insert into Exemplar values(7, '2021/02/24', '9783527760701');
insert into Exemplar values(8, '2016/06/10', '9788576089964');
insert into Exemplar values(9, '2019/01/08', '9788575227183');
insert into Exemplar values(10, '2013/01/01', '9788521207009');

select * from Exemplar;

CREATE TABLE IF NOT EXISTS Autoria (
	autorCodigo int not null,
    livroISBN char(13),
    constraint fk_Autor_codigo foreign key (autorCodigo) references Autor (codigo),
    constraint fk_Livro_ISBN foreign key (livroISBN) references Livro (ISBN)
);

--    autorCodigo, livroISBN
insert into Autoria values(1, '9788535212730');
insert into Autoria values(3, '9788575226919');
insert into Autoria values(4, '9788576089964');
insert into Autoria values(5, '9788575227183');
insert into Autoria values(6, '9781305109919');
insert into Autoria values(7, '9788521207009');
insert into Autoria values(8, '9780596100292');
insert into Autoria values(9, '9788536531458');
insert into Autoria values(10, '9788576089452');
insert into Autoria values(10, '9783527760701');

select * from Autoria;


CREATE TABLE IF NOT EXISTS Cliente (
	codigo int,
    nome varchar(100) not null,
    email varchar(100),
    rg char(9) not null,
    rua varchar(100),
    cep char(8) not null,
    bairro varchar(100),
    constraint pk_Cliente primary key (codigo),
    unique (rg)
);

--    codigo, nome, email, rg, rua, cep, bairro
insert into Cliente values(1, 'Genésia Perdigão Trindade', 'ganesia1996@email.com', '911225341', 'Rua Antenor Gomes de Oliveira', '57055265', 'Farol');
insert into Cliente values(2, 'Célio Caires Sobral', 'celio_caires@email.com', '130148908', 'Rua Agostinho Garcia Lobo', '58101412', 'Poço');
insert into Cliente values(3, 'Kushal Raminhos Figueiredo', 'kushal.raminhos@email.com', '257899753', 'Rua Professora Maria Augusta Mendonça', '58433238', 'Malvinas');
insert into Cliente values(4, 'Zara Nogueira Lameiras', 'zarinha@email.com', '110376626', 'Travessa Jesuíno Alves Correia', '58415348', 'Cruzeiro');
insert into Cliente values(5, 'Ava Saldanha Dias', 'avadias@email.com', '447445157', 'Travessa do Cajueiro', '58100516', 'Ponta de Matos');
insert into Cliente values(6, 'Sebastião Serralheiro Aveiro', 'sebastiao12@email.com', '269676831', 'Rua Professora Gercina Coutinho de Carvalho', '58034060', 'Brisamar');
insert into Cliente values(7, 'Selma Guerreiro Banha', 'selmaguerreiro@email.com', '220676331', 'Vila das Graças', '58070003', 'Varjão');
insert into Cliente values(8, 'Eden Varão Canhão', 'eden_varao@email.com', '174326026', 'Rua José Ailton Pessoa Gonçalves', '58084520', 'Jardim Veneza');
insert into Cliente values(9, 'Nuna Castanheira Covinha', 'nuna.castanheira@email.com', '185138688', 'Rua Suerda Pacote', '58059125', 'Mangabeira');
insert into Cliente values(10, 'Kyara Rufino Cerqueira', 'kyararufino@email.com', '434591476', 'Rua Maria Augusta Pereira', '58423766', 'Três Irmãs');

select * from Cliente;

CREATE TABLE IF NOT EXISTS Emprestimo (
	dataEmprestimo date,			-- YYYY/MM/DD
    clienteCodigo int not null,		-- criar foreign key
    exemplarCodigo int not null,	-- criar foreign key
    constraint pk_Emprestimo primary key (dataEmprestimo),
    constraint fk_Cliente_codigo foreign key (clienteCodigo) references Cliente (codigo),
	constraint fk_Exemplar_codigo foreign key (exemplarCodigo) references Exemplar (codigo)
);

--    dataEmprestimo, clienteCodigo, exemplarCodigo
insert into Emprestimo value('2019/07/15', 4, 1);
insert into Emprestimo value('2017/07/22', 6, 1);
insert into Emprestimo value('2016/06/10', 10, 4);
insert into Emprestimo value('2020/02/24', 9, 3);
insert into Emprestimo value('2016/04/29', 5, 5);
insert into Emprestimo value('2013/01/01', 8, 10);
insert into Emprestimo value('2018/08/10', 7, 6);
insert into Emprestimo value('2017/10/01', 1, 8);
insert into Emprestimo value('2014/06/23', 3, 4);
insert into Emprestimo value('2019/12/11', 2, 9);

select * from Emprestimo;

-- Consultas:

-- Retorne todos os dados dos clientes p/ aqueles que começam com a letra 'M'.
SELECT c.nome FROM Cliente c WHERE nome LIKE "M%";

-- Retorne o 'nome', 'rg' (Cliente) e data da aquisição do exemplar (Exemplares);
SELECT c.nome, c.rg, ex.dataAquisicao FROM Emprestimo e INNER JOIN Cliente c INNER JOIN Exemplar ex 
ON e.clienteCodigo = c.codigo AND e.exemplarCodigo = ex.codigo;

-- Retorne a quantidade total de livros cadastrados;
SELECT count(*) AS 'Quantidade Total de Livros' FROM Livro;

-- Retorne o 'nome' (Editora), a 'edição' (Livro) e o 'título' (Livro) para aqueles que custam (Livro) mais de cem reais ('custo' > 100).
SELECT e.nome 'Nome Editora', l.edicao 'Edição do Livro', l.titulo 'Título' FROM Livro l INNER JOIN Editora e ON l.codigoEditora  = e.codigo WHERE l.custo > 100;

-- Retorne a quantidade de livros por edição;
SELECT l.edicao, count(*) 'Quantidade' FROM Livro l GROUP BY l.edicao;

-- Crie uma visão para trazer o 'nome' (Cliente), 'e-mail' (Cliente) e 'endereço' (Cliente), 'data' (Emprestimo) e 'data' de aquisição (Exemplar).
CREATE VIEW v_controle_livraria AS SELECT c.nome, c.email, c.rua, c.cep, c.bairro, e.dataEmprestimo 'Emprestimo', ex.dataAquisicao 'Aquisição da Livraria' 
FROM Emprestimo e INNER JOIN Cliente c INNER JOIN Exemplar ex 
ON e.clienteCodigo = c.codigo AND e.exemplarCodigo = ex.codigo;

-- Retorne o 'nome' (Autor), 'titulo' e 'edicao' (Livro) para aquele cujo custo foi o mais em conta.
SELECT a.nome,  l.titulo, l.edicao, MIN(l.custo) 'Livro mais barato' FROM Livro l INNER JOIN Autoria au INNER JOIN Autor a 
ON au.autorCodigo = a.codigo AND au.livroISBN = l.ISBN GROUP BY a.codigo;

-- Retorne o 'codigo' e 'data' de aquisição (Exemplar) para aqueles que a data for agosto/2020 (01~31).
SELECT ex.codigo, ex.dataAquisicao FROM Exemplar ex WHERE ex.dataAquisicao BETWEEN '2020/08/01' AND '2020/08/31';

-- Retorne o 'ISBN', 'custo' e 'titulo' (Livro), para aqueles que são de 'edicao' (Livro) 1, 3 e 4.
SELECT l.ISBN, l.custo, l.titulo FROM Livro l WHERE l.edicao IN (1,3,4);

-- Retorne o 'nome' (Editora), 'titulo' (Livro) e 'nome' (Autor).
SELECT e.nome 'Nome da Editora', l.titulo 'Nome do Livro', a.nome 'Nome Autor' FROM Editora e INNER JOIN Livro l INNER JOIN Autor a INNER JOIN Autoria au 
ON au.autorCodigo = a.codigo AND au.livroISBN = l.ISBN AND l.codigoEditora = e.codigo;
