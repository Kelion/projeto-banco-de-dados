create database livraria;
use livraria;

create table Cliente(
	codigo int,
    nome varchar(100) not null,
    email varchar(100),				-- pode conter 1+
    rg char(8) not null,			-- somente números
    endereco varchar(100),
    primary key(codigo),
    unique(rg)
);

create table Autores(
	codigo int,
    email varchar(100),				-- pode conter 1+
    nome varchar(100) not null,
    primary key(codigo)
);

create table Editora(
	codigo int,
    nome varchar(50) not null,
    primary key(codigo),
    unique(nome)
);

create table Livros(
	ISBN char(7),
    edicao int,
    custo decimal(7,2) not null,
    titulo varchar(100) not null,
    cod_ed int not null,			-- criar foreign key
    cod_aut int not null,			-- criar foreign key
    primary key(ISBN),
    foreign key(cod_ed) references Editora(codigo),
    foreign key(cod_aut) references Autores(codigo)
);

create table Exemplares(
	codigo int,
    dataAquisicao date,				-- YYYY/MM/DD
    cod_cli int not null,			-- criar foreign key
    isbn_li char(7) not null,		-- criar foreign key
    primary key(codigo),
    foreign key(cod_cli) references Cliente(codigo),
    foreign key(isbn_li) references Livros(ISBN)
);